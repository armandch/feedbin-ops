import { default as axios} from 'axios';
import { AUTH_TOKEN } from "./auth.js";

const tagName = 'developers';

const getSubscriptionAndTaggingInfo = async () => {
    let subscriptionAndTaggingInfo = {};

    let resSubs = await axios.get('https://api.feedbin.com/v2/subscriptions.json', {
        headers: {
            'Authorization': `Basic ${AUTH_TOKEN}`
        }
    });
    subscriptionAndTaggingInfo.subscriptions = resSubs.data;

    let resTags = await axios.get('https://api.feedbin.com/v2/taggings.json', {
        headers: {
            'Authorization': `Basic ${AUTH_TOKEN}`
        }
    });
    subscriptionAndTaggingInfo.taggingsList = resTags.data;

    return subscriptionAndTaggingInfo;
}

const doRun = async () => {
    getSubscriptionAndTaggingInfo().then(subAndTagsInfo => {
        let subsAndTagsSet = new Set();
        for (const tagging of subAndTagsInfo.taggingsList) {
            subsAndTagsSet.add(tagging.feed_id);
        }

        for (const sub of subAndTagsInfo.subscriptions) {
            if (!subsAndTagsSet.has(sub.feed_id)) {
                axios.post('https://api.feedbin.com/v2/taggings.json', {
                    feed_id: sub.feed_id,
                    name: tagName
                }, {
                    headers: {
                        'Authorization': `Basic ${AUTH_TOKEN}`
                    }
                }).then(response => {
                    console.log('Tagging created: ' + JSON.stringify(response.data));
                });
            }
        }
    });
};

doRun().then(result => {

});