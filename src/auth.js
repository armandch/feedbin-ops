const USERNAME = '';
const PASSWORD = '';

const AUTH_TOKEN = Buffer.from(`${USERNAME}:${PASSWORD}`, 'utf8',).toString('base64');

export { AUTH_TOKEN };