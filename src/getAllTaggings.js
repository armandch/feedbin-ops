import { default as axios} from 'axios';
import { AUTH_TOKEN } from "./auth.js";

const getAllTaggings = async () => {
    let resTags = await axios.get('https://api.feedbin.com/v2/taggings.json', {
        headers: {
            'Authorization': `Basic ${AUTH_TOKEN}`
        }
    });
    return resTags.data;
};

const doRun = async () => {
    getAllTaggings().then(taggings => {
        let taggingsSet = new Set();
        for (const tagging of taggings) {
            taggingsSet.add(tagging.name);
        }
        for (const taggingName of taggingsSet) {
            console.log(taggingName);
        }
    });
};
doRun().then(result => {

});