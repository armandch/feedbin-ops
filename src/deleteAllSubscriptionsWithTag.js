import { default as axios} from 'axios';
import { AUTH_TOKEN } from "./auth.js";

const taggingName = 'developers';

const getSubscriptionAndTaggingInfo = async () => {
    let subscriptionAndTaggingInfo = {};

    let resSubs = await axios.get('https://api.feedbin.com/v2/subscriptions.json', {
        headers: {
            'Authorization': `Basic ${AUTH_TOKEN}`
        }
    });
    subscriptionAndTaggingInfo.subscriptions = resSubs.data;

    let resTags = await axios.get('https://api.feedbin.com/v2/taggings.json', {
        headers: {
            'Authorization': `Basic ${AUTH_TOKEN}`
        }
    });
    subscriptionAndTaggingInfo.taggingsList = resTags.data;

    return subscriptionAndTaggingInfo;
}

const doRun = async () => {
    getSubscriptionAndTaggingInfo().then(subAndTagsInfo => {
        let taggingsMap = new Map();
        for (const tagging of subAndTagsInfo.taggingsList) {
            taggingsMap.set(tagging.feed_id, tagging.name);
        }

        for (const sub of subAndTagsInfo.subscriptions) {
            if (taggingName == taggingsMap.get(sub.feed_id)) {
                axios.delete('https://api.feedbin.com/v2/subscriptions/' + sub.id + '.json', {
                    headers: {
                        'Authorization': `Basic ${AUTH_TOKEN}`
                    }
                });
                console.log('Feed: ' + sub.title + ' deleted.');
            }
        }
    });
};

doRun().then(result => {

});